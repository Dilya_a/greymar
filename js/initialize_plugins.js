//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins START  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/




		var styler 			= $(".styler"),
			wow 			= $(".wow"),
			popup 			= $("[data-popup]"),
			swiperSlider 	= $(".swiper_carousel"),
			swiperSlider2 	= $(".partners_carousel"),
			swiperSlider3 	= $(".reviews_slider"),
			counter 		= $(".counter_value"),
			accordion 		= $('.accordion');


			if(swiperSlider.length || swiperSlider2.length || swiperSlider3.length){
					include("plugins/swiper.js");
			}
			if(accordion.length){
					include("plugins/jquery-ui.js");
			}
			if(styler.length){
					include("plugins/formstyler/formstyler.js");
			}
			if(wow.length){
					include("plugins/wow.min.js");
			}
			if(popup.length){
					include('plugins/arcticmodal/jquery.arcticmodal.js');
			}

					include("plugins/modernizr.js");



			function include(url){ 

					document.write('<script src="'+ url + '"></script>'); 

			}

		


		$(document).ready(function(){

			/* ------------------------------------------------
				Counter
			------------------------------------------------ */

			if(counter.length){

				counter.each(function() {
				  var $this = $(this),
				      countTo = $this.attr('data-count');
				  
				  $({ countNum: $this.text()}).animate({
				    countNum: countTo
				  },

				  {

				    duration: 1500,
				    easing:'linear',
				    step: function() {
				      $this.text(Math.floor(this.countNum));
				    },
				    complete: function() {
				      $this.text(this.countNum);
				      //alert('finished');
				    }

				  });  
				  
				  

				});
			}

			/* ------------------------------------------------
				Accordion
			------------------------------------------------ */

			if(accordion.length){

				accordion.accordion({
					heightStyle: "content"
				});
			}


			/* ------------------------------------------------
			SWIPER SLIDER START
			------------------------------------------------ */

					if (swiperSlider.length){
						var swiper = new Swiper('.swiper_carousel', {
					        pagination: '.swiper_carousel .swiper-pagination',
					        paginationClickable: true,
					        nextButton: '.swiper-button-next',
					        prevButton: '.swiper-button-prev',
					        spaceBetween: 30,
					        effect: 'fade'
					    });
					};

					if (swiperSlider2.length){
						var swiper = new Swiper('.partners_carousel', {
					        pagination: '.partners_carousel .swiper-pagination',
					        paginationClickable: true,
					        // nextButton: '.swiper-button-next',
					        // prevButton: '.swiper-button-prev',
					        spaceBetween: 30,
					        slidesPerView: 5,

					    });
					};

					if (swiperSlider3.length){
						var swiper = new Swiper('.reviews_slider', {
					        pagination: '.reviews_slider .swiper-pagination',
					        paginationClickable: true,
					        nextButton: '.swiper-button-next',
					        prevButton: '.swiper-button-prev',
					        spaceBetween: 30
					    });
					};

			/* ------------------------------------------------
			SWIPER SLIDER END
			------------------------------------------------ */


			/* ------------------------------------------------
			FORMSTYLER START
			------------------------------------------------ */

					if (styler.length){
						styler.styler({
							// selectSmartPositioning: true
						});
					}

			/* ------------------------------------------------
			FORMSTYLER END
			------------------------------------------------ */




			/* ------------------------------------------------
			ANIMATE block START
			------------------------------------------------ */

					if(wow.length){
				        if($("html").hasClass("md_no-touch")){
							new WOW().init();	
						}
						else if($("html").hasClass("md_touch")){
							$("body").find(".wow").css("visibility","visible");
						}

					}

			/* ------------------------------------------------
			ANIMATE block END
			------------------------------------------------ */




			/* ------------------------------------------------
			POPUP START
			------------------------------------------------ */

					if(popup.length){
						popup.on('click',function(){
						    var modal = $(this).data("popup");
						    $(modal).arcticmodal();
						});
					};

			/* ------------------------------------------------
			POPUP END
			------------------------------------------------ */




		});




//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins END    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================
